/**
 * Article.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const dateFormatter = require('moment');

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  //schema: true,

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      //required: true
    },
    name :{
      type: 'string',
      size: 255,
      required: true,
      defaultsTo: 'Без имени'
    },
    content: {
      type:'longtext',
      required: true,
      defaultsTo: ' '
    },
    postDate: {
      type:'datetime',
      required: true,
      defaultsTo: function(){
        return dateFormatter().format("YYYY-MM-DD HH:mm:ss")
      }
    },

    author: {
      model: 'user'
    },

    categories: {
      collection: 'category',
      via: 'articles',
      defaultsTo : [1]
    }
  }
};

