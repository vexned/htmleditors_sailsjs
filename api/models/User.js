/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {

  connection: 'someMysqlServer',
  autoPK: false,
  autoCreatedAt: false,
  autoUpdatedAt: false,
  //schema: true,

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      //required: true
    },
    login: {
      type: 'string',
      required: true,
      size: 255,
      unique: true
    },
    password: {
      type: 'string',
      required: true,
      size: 255
    },
    name: {
      type: 'string',
      size: 255,
      defaultsTo: 'Иван'
    },
    lastname: {
      type: 'string',
      size: 255,
      defaultsTo: 'Иванов'
    },
    email: {
      type: 'email',
      required: true,
      unique: true
    },

    usergroup: {
      model: 'usergroup',
      required: true,
      defaultsTo: [1]
    },

    toJSON: function () {
      var obj = this.toObject();
      delete obj._csrf;
      delete obj.password;
      return obj;
    } 
  },
  beforeCreate: function (values, cb) {
    
          // Hash password
          bcrypt.hash(values.password, saltRounds, function (err, hash) {
            if (err) return cb(err);
            values.password = hash;
            //calling cb() with an argument returns an error. Useful for canceling the entire operation if some criteria fails.
            cb();
          });
  }
};

