/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const bcrypt = require('bcrypt');

module.exports = {
	'register': function(req, res) {
        res.view('user/register');
    },
    'add': function(req, res) {
        UserGroup.find({}).exec(function(err, usergroup) {
            if(err) return next(err);
            res.view('user/add', {usergroup:usergroup});
        });
    },
    'performRegister': function(req, res) {
        
        User.create(req.params.all(), function(err, user){
            if(err) { 
                req.session.flash = {
                    err: ["Введены неверные данные при регистрации!"]
                }
                return res.redirect('/user/register')
            }
            
            res.redirect('/');
        })
    },
    'new': function(req, res) {
        
        User.create(req.params.all(), function(err, user){
            if(err) { 
                req.session.flash = {
                    err: ["Введены неверные данные при регистрации!"]
                }
                return res.redirect('/user/add')
            }
            
            res.redirect('user/show');
        })
    },
    'edit': function(req, res) {
        
        User.findOne(req.params.id).exec(function(err, user){
            if(err) return next(err);
            if(!user) return next();
            UserGroup.find({}).exec(function(err, usergroup){
                if(err) return next(err);
                res.view('user/edit',{user:user, usergroup:usergroup});
            })
        });
    },
    'update': function(req, res) {
        params = req.params.all();
        console.log(params);
        User.update({id: params.id}, req.params.all()).exec(function(err, updated){
            if(err) return next(err);

            res.redirect('user/show');
        });
    },
    'login': function(req, res) {

        params = req.params.all();
        //console.log(pms);
        //console.log(req.params.all());

        let login = params.login;
        let password = params.password;

        if(!login || !password)
        {
            req.session.flash = {
                err: ['Все поля должны быть заполнены!']
            }
            res.redirect('/');
            return;
        }

        User.findOne({
            login: login
        }).exec(function (err, user){
            if(err) return next(err);
            if(!user) {
                req.session.flash = {
                    err: ['Пользователя с таким именем не существует!']
                }
                res.redirect('/');
                return;
            }

            bcrypt.compare(password, user.password, function(err, valid) {
                if(err) return next(err);

                if(!valid){
                    req.session.flash = {
                        err: ['Такой комбинации логина и пароля нет!']
                    }
                    res.redirect('/');
                    return;
                }
            });

            req.session.authenticated = true;
            req.session.User = user;

            res.redirect('/');

        })
    },
    'exit':function(req, res) {
        req.session.destroy();
        res.redirect('/');
    },
    'delete': function(req, res) {
        params = req.params.all();
        console.log(params);
        User.destroy({id: params.id}).exec(function(err){
            if(err) return next(err);

            res.redirect('user/show');
        });
    },
    'show': function(req, res) {
        User.find({}).populate('usergroup').exec(function(err, result){
            if(err) return next(err);
            console.log(result)
            res.view('user/show', {users: result});
        });
    },
    'profile': function(req, res) {
        User.findOne({id:req.params.id}).populate('usergroup').exec(function(err, result){
            if(err) return next(err);
            res.view('user/profile', {user: result});
        });
    },
    'me': function(req, res) {
        console.log(req.session);
        User.findOne({id:req.session.User.id}).populate('usergroup').exec(function(err, result){
            if(err) return next(err);
            res.view('user/profile', {user: result});
        });
    },
};

