/**
 * UserGroupController
 *
 * @description :: Server-side logic for managing usergroups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'show': function(req, res) {
        UserGroup.find({}).exec(function(err, result){
            if(err) return next(err);

            res.view('usergroup/show', {usergroup: result});
        });
    },
    'edit': function(req, res) {
        UserGroup.findOne({id:req.params.id}).exec(function(err, result){
            if(err) return next(err);

            res.view('usergroup/edit', {usergroup: result});
        });
    },
    'update': function(req, res) {
        params = req.params.all();
        console.log(params);
        UserGroup.update({id: params.id}, req.params.all()).exec(function(err, updated){
            if(err) return next(err);

            res.redirect('usergroup/show');
        });
    },
    'delete': function(req, res) {
        params = req.params.all();
        console.log(params);
        UserGroup.destroy({id: params.id}).exec(function(err){
            if(err) return next(err);

            res.redirect('usergroup/show');
        });
    },
    'add':function(req, res){
        res.view('usergroup/add');
    },
    'new':function(req, res){
        params = req.params.all();
        UserGroup.create(params).exec(function(err, result){
            if(err) { 
                req.session.flash = {
                    err: err
                }
                return res.redirect('/usergroup/add')
            }
            
            res.redirect('usergroup/show');
        });
    }
};

