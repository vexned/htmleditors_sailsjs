/**
 * ArticleController
 *
 * @description :: Server-side logic for managing articles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment = require('moment');

module.exports = {
	'add': function(req, res) {
        Category.find({}).exec(function(err, category){
            if(err) return next(err);
            if(!category) return next();

            res.view('article/add',{category:category});
        });
    },
    'new': function(req, res) {
        params = req.params.all();
        params.author = req.session.User.id;
        Article.create(params, function(err, user) {
            if(err) { 
                req.session.flash = {
                    err: err
                }
                return res.redirect('/article/add')
            }
            
            res.redirect('/');
        });

    },
    'edit': function(req, res) {
        Article.findOne(req.params.id).populate('categories').exec(function(err, article){
            if(err) return next(err);
            if(!article) return next();
            Category.find({}).exec(function(err, category){
                if(err) return next(err);
                if(!category) return next();
                var cats = [];
                article.categories.forEach(function(cat, i, arr) {
                    cats.push(cat.id);
                });
                console.log(cats);
                res.view('article/edit',{category:category, article:article, cats:cats});
                })
            });
    },
    'update': function(req, res){
        params = req.params.all();
        console.log(params);
        Article.update({id: params.id}, req.params.all()).exec(function(err, updated){
            if(err) return next(err);

            res.redirect('/');
        });
    },

    'show': function(req, res){
        Article.find({}).populate('categories').populate('author').exec(function(err, articles){
            if(err) return next(err);
            res.view('homepage', {articles:articles, moment:moment});
        });
    },
    'delete': function(req, res){
        Article.destroy({id: req.params.id}).exec(function(err, result){
            if(err) return res(err);

            res.redirect('/');
        });
    }
};

