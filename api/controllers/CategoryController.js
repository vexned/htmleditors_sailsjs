/**
 * CategoriesController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment = require('moment');

module.exports = {
	'show': function(req, res) {
        Category.find({}).populate('articles').exec(function(err, result){
            if(err) return next(err);

            res.view('category/show', {category: result});
        });
    },
    'edit': function(req, res) {
        Category.findOne({id:req.params.id}).exec(function(err, result){
            if(err) return next(err);

            res.view('category/edit', {category: result});
        });
    },
    'update': function(req, res) {
        params = req.params.all();
        console.log(params);
        Category.update({id: params.id}, req.params.all()).exec(function(err, updated){
            if(err) return next(err);

            res.redirect('category/show');
        });
    },
    'delete': function(req, res) {
        params = req.params.all();
        console.log(params);
        Category.destroy({id: params.id}).exec(function(err){
            if(err) return next(err);

            res.redirect('category/show');
        });
    },
    'add':function(req, res){
        res.view('category/add');
    },
    'new':function(req, res){
        params = req.params.all();
        Category.create(params).exec(function(err, result){
            if(err) { 
                req.session.flash = {
                    err: err
                }
                return res.redirect('/category/add')
            }
            
            res.redirect('category/show');
        });
    },
    'showby':function(req, res) {
        Category.findOne(req.params.id).populate('articles').exec(function(err, category){
            if(err) return next(err);
            res.view('category/showby',{category:category, moment:moment});
        });
    }
};

