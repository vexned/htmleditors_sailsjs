/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': 'ArticleController.show',
  '/user/register': 'UserController.register',
  '/user/performRegister': 'UserController.performRegister',
  'POST /user/login': 'UserController.login',
  '/user/edit/:id':'UserController.edit',
  '/user/delete/:id':'UserController.delete',
  '/user/update/:id':'UserController.update',
  '/user/add':'UserController.add',
  '/user/show':'UserController.show',
  '/user/exit': 'UserController.exit',
  '/user/:id':'UserController.profile',
  '/profile':'UserController.me',

  '/article/add': 'ArticleController.add',
  '/article/new': 'ArticleController.new',
  '/article/edit/:id':'ArticleController.edit',
  '/article/update/:id':'ArticleController.update',
  '/article/delete/:id':'ArticleController.delete',

  '/category/update/:id':'CategoryController.update',
  '/category/edit/:id':'CategoryController.edit',
  '/category/delete/:id':'CategoryController.delete',
  '/category/add':'CategoryController.add',
  '/category/show':'CategoryController.show',
  '/category/:id':'CategoryController.showby',

  '/usergroup/update/:id':'UserGroupController.update',
  '/usergroup/edit/:id':'UserGroupController.edit',
  '/usergroup/delete/:id':'UserGroupController.delete',
  '/usergroup/add':'UserGroupController.add',
  '/usergroup/show':'UserGroupController.show',


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
